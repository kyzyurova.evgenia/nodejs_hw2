const { User } = require('./models/Users.js');
const bcrypt = require('bcryptjs');

function userInfo (req, res, next) {
    User.findById(req.user.userId)
        .then((user) => {
          res.json({message: 'Success', user});
        });
}

const changePass = async(req, res, next) => {
  const { oldPassword, newPassword } = req.body;
  try{
      if(!oldPassword){
          throw new Error('Old Password is empty')
      }else if(!newPassword){
        throw new Error('New Password is empty')
    }
    const password = await bcrypt.hash(newPassword, 10);
      User.findByIdAndUpdate({_id: req.user.userId, password: oldPassword}, {$set: { password:  password} })
          .then((result) => {
              if(result === null){
                  throw new Error('User was not found');
              }else{
                  res.json({message: 'Success', result});
              }
          });
      }catch(err){
          res.status(400).send({ message: err.message });
      }
  
}


const deleteAcc = (req, res, next) => {
  try{
      User.findByIdAndDelete({_id: req.user.userId})
          .then((result) => {
              if(result === null){
                  throw new Error('Note was not found');
              }else{
                  res.json({message: 'Success', result});
              }
          });
      }catch(err){
          res.status(400).send({ message: err.message });
      }
}

module.exports = {
    userInfo,
    changePass,
    deleteAcc
  };