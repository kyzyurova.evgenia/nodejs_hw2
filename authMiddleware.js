const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
  const  {
    authorization
  } = req.headers;

  if (!authorization) {
    return res.status(401).json({ 'message': 'Please, provide authorization header' });
  }

  const [, jwt_token] = authorization.split(' ');

  if (!jwt_token) {
    return res.status(401).json({ 'message': 'Please, include token to request' });
  }

  try {
    const tokenPayload = jwt.verify(jwt_token, 'secretTokenKey');
    req.user = {
      userId: tokenPayload.userId,
      username: tokenPayload.username,
      name: tokenPayload.name
    }
    next();
  } catch (err) {
    return res.status(401).json({message: err.message});
  }

}

module.exports = {
  authMiddleware
}
