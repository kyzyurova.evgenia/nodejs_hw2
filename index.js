const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://evgeniaAdmin:m0n9b8v7c6x5z4@todoapp.m6jlf3v.mongodb.net/?retryWrites=true&w=majority');

const { notesRouter } = require('./notesRouter.js');
const { usersRouter } = require('./usersRouter.js');
const { profileRouter } = require('./profileRouter.js');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/notes', notesRouter);
app.use('/api/auth', usersRouter);
app.use('/api/users', profileRouter);

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}
