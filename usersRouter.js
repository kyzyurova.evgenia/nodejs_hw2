const express = require('express');
const router = express.Router();
const { registerUser } = require('./registerUser.js');
const  { loginUser } = require('./loginUser.js');
const { authMiddleware } = require('./authMiddleware');

router.post('/register', registerUser);

router.post('/login', loginUser);

module.exports = {
  usersRouter: router,
};
