// requires...
const { Note } = require('./models/Notes.js');
// constants...

function createNote (req, res, next) {
    const { text } = req.body;
    try{
        const note = new Note({
            text,
            userId : req.user.userId
          });
        if(!text){
            throw new Error ('text is empty');
        }else{
            note.save()
            .then(saved => res.json({saved, message: 'Success'}))
            .catch(err => {
              next(err);
              });
        } 
    }catch(err){
        console.error(err.message);
        res.status(400).send({ message: err.message });
    }
    
  
}

const getNoteById = (req, res, next) => {
    try{
        
        Note.findById(req.params.id)
            .then((result) => {
                if(result === null){
                    throw new Error ('Note was not found');
                    
                }else{
                    res.json({message: 'Success', note: result});
                }
            
            });
    }catch(err){
        res.status(400).send({ message: err.message });
    }
  
}
const getNotesOfUser = (req, res, next) => {
    try{
        Note.find({userId: req.user.userId}, '-__v')
        .then(result =>{
            const count = result.length;
            const { offset = 0, limit = count } = req.query;
            console.log(offset, limit, count);
            Note.find({userId: req.user.userId}, '-__v').skip(offset).limit(limit)
                .then((result) => {
                    if(result === null){
                        throw new Error('Notes were not found');
                    }else{
                        res.json({message: 'Success',offset, limit, count, notes: result});
                    }
                });
        });
        
        
    }catch(err){
        res.status(400).send({ message: err.message });
    }
    
}

const markNoteCompletedById = (req, res, next) => {
    try {
        Note.findByIdAndUpdate({_id: req.params.id, userId: req.user.userId}, {$set: { completed: true } })
            .then((result) => {
                if(result === null){
                    throw new Error('Note was not found');
                }else{
                    Note.findById(result.id)
                        .then((result) => {res.json({message: 'Success', result})});
                }
              });
    }catch(err){
        res.status(400).send({ message: err.message });
    }
}

const updateMyNoteById = (req, res, next) => {
    const { text} = req.body;
    try{
        if(!text){
            throw new Error('Text is empty')
        }
        Note.findByIdAndUpdate({_id: req.params.id, userId: req.user.userId}, {$set: { text: text } })
            .then((result) => {
                if(result === null){
                    throw new Error('Note was not found');
                }
                Note.findById(result.id)
                    .then((result) => {res.json({message: 'Success', text: result.text});})
                    });
        }catch(err){
            res.status(400).send({ message: err.message });
        }
        
    
}

  
const deleteNote = (req, res, next) => {
    try{
        Note.findByIdAndDelete({_id: req.params.id, userId: req.user.userId})
            .then((result) => {
                if(result===null){
                    throw new Error('Note was not found');
                }else{
                    res.json({message: 'Success'});
                }
            });
        }catch(err){
            res.status(400).send({ message: err.message });
        }
}



module.exports = {
  createNote,
  getNoteById,
  getNotesOfUser,
  markNoteCompletedById,
  updateMyNoteById,
  deleteNote
}
