const { User } = require('./models/Users.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const loginUser = async (req, res, next) => {
  try{
    const userName = req.body.username;
    const userPass = req.body.password;
    if(!userName){
      throw new Error('Username is empty')
    }else if(!userPass){
      throw new Error('Password is empty')
    }
    const user = await User.findOne({ username: userName });
    if(!user){
      throw new Error('User is not registered')
    }
    const isPassCorrect = await bcrypt.compare(String(userPass), String(user.password));
    if(!isPassCorrect){
      throw new Error('Password is not correct')
    }
    if (user && isPassCorrect) {
      const payload = { username: user.username, name: user.name, userId: user._id };
      const jwtToken = jwt.sign(payload, 'secretTokenKey');
      return res.json({message: 'Success', jwt_token: jwtToken});
    }
    return res.status(403).json({'message': 'Not authorized'});
  }catch(err){
    console.error(err.message);
    res.status(400).send({ message: err.message });
  }
    
}
  
module.exports = {
  loginUser
};