const express = require('express');
const router = express.Router();
const  { userInfo, changePass, deleteAcc } = require('./userInfo.js');
const { authMiddleware } = require('./authMiddleware');

router.get('/me', authMiddleware, userInfo);
router.patch('/me', authMiddleware, changePass);
router.delete('/me', authMiddleware, deleteAcc);

module.exports = {
  profileRouter: router,
};