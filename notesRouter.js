const express = require('express');
const router = express.Router();
const { createNote, getNoteById, getNotesOfUser, markNoteCompletedById, updateMyNoteById, deleteNote } = require('./notesService.js');
const { authMiddleware } = require('./authMiddleware');

router.post('/', authMiddleware, createNote);

router.get('/', authMiddleware, getNotesOfUser);

router.get('/:id', authMiddleware, getNoteById);

router.patch('/:id', authMiddleware, markNoteCompletedById);

router.put('/:id', authMiddleware, updateMyNoteById);

router.delete('/:id', authMiddleware, deleteNote);

module.exports = {
  notesRouter: router,
};
