const { User } = require('./models/Users.js');
const bcrypt = require('bcryptjs');

const registerUser = async (req, res, next) => {
  
  try{
    const { name, username, password } = req.body;
    if(!username){
      throw new Error('Username is empty')
    }else if(!password){
      throw new Error('Password is empty')
    }
    const user = new User({
      name,
      username,
      password: await bcrypt.hash(password, 10)
    });
  
    user.save()
      .then(saved => res.json({saved, message: 'Success'}))
      .catch(err => {
        next(err);
      });
  }catch(err){
    res.status(400).send({ message: err.message });
  }
  
}

module.exports = {
  registerUser
};
